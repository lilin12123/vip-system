package com.xbjtech.vipsystem;

import com.xbjtech.vipsystem.po.Role;
import com.xbjtech.vipsystem.po.User;
import com.xbjtech.vipsystem.service.RoleService;
import com.xbjtech.vipsystem.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VipsystemApplicationTests {

    @Autowired
    UserService userService;

    @Test
    public void contextLoads() {
        List<User> users = userService.selectAll();
        System.out.println(Arrays.toString(users.toArray()));
    }

    @Autowired
    RoleService roleService;

    @Test
    public void insertRoles() {
        for (int i = 0; i < 101; i++) {
            Role role = new Role();
            role.setRoleName("管理员" + i);
            role.setRoleCode("admin" + i);
            role.setDescription("描叙" + i);
            roleService.insertWithoutPrimaryKey(role);
        }
    }

}
