package com.xbjtech.vipsystem.utils;

import com.xbjtech.vipsystem.po.Resource;
import io.lettuce.core.output.StatusOutput;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-24
 */
public class ArgumentResolver<T> {

    public T resolver(T basic, T request, Class<T> clazz) {
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("set")) {
                String getMethodName = method.getName().replaceFirst("set", "get");
                try {
                    Method getMethod = clazz.getMethod(getMethodName);
                    getMethod.setAccessible(true);
                    Object getMethodReturnValue = getMethod.invoke(request);
                    if (getMethodReturnValue != null) {
                        method.setAccessible(true);
                        method.invoke(basic, getMethodReturnValue);
                    }
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return basic;
    }

    public static void main(String[] args) {
        Resource basic = new Resource();
        basic.setId(1);
        basic.setResName("用户管理");
        basic.setIcon("iconstring");

        /**
         * {
         *   "id": 10,
         *   "title": "用户列表"
         * }
         */
        Resource request = new Resource();
        request.setId(1);
        basic.setResName("用户列表");

        ArgumentResolver<Resource> resolver = new ArgumentResolver<>();
        Resource b = resolver.resolver(basic, request, Resource.class);
        System.out.println(b);
    }

}
