package com.xbjtech.vipsystem.shiro;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-23
 */
@Data
public class JwtToken implements AuthenticationToken {

    private String token;

    public JwtToken() {}

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return this.token;
    }

    @Override
    public Object getCredentials() {
        return this.token;
    }
}
