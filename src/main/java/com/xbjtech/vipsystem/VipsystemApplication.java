package com.xbjtech.vipsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "com.xbjtech.vipsystem.mapper")
public class VipsystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(VipsystemApplication.class, args);
    }

}
