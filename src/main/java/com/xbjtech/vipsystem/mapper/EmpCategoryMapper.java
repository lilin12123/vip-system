package com.xbjtech.vipsystem.mapper;

import com.xbjtech.vipsystem.po.EmpCategory;

public interface EmpCategoryMapper extends BaseMapper<EmpCategory> {
}