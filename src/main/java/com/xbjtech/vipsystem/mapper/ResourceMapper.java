package com.xbjtech.vipsystem.mapper;

import com.xbjtech.vipsystem.po.Resource;
import com.xbjtech.vipsystem.vo.SelectTreeData;

import java.util.List;

public interface ResourceMapper extends BaseMapper<Resource> {

    List<SelectTreeData> selectTreeDate();

    List<SelectTreeData> selectTreeDataThreeLevel();

}