package com.xbjtech.vipsystem.mapper;

import tk.mybatis.mapper.annotation.RegisterMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @RegisterMapper 标识当前接口是一个通过的Mapper接口，并不是一个需要代理的Mapper接口
 * @author leejun.happy@gmail.com
 * @created 2019-07-15
 */
@RegisterMapper
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
