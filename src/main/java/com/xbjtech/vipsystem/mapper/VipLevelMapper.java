package com.xbjtech.vipsystem.mapper;


import com.xbjtech.vipsystem.po.VipLevel;

public interface VipLevelMapper extends BaseMapper<VipLevel> {
}