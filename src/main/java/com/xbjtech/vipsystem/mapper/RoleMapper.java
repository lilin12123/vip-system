package com.xbjtech.vipsystem.mapper;


import com.xbjtech.vipsystem.po.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper extends BaseMapper<Role> {

    int insertRoleAndResRelation(
            @Param("roleId") Integer roleId,
            @Param("resId") Integer resId);

    int deleteRoleRelation(Integer roleId);

    List<Integer> selectResByRoleId(Integer roleId);
}