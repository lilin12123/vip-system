package com.xbjtech.vipsystem.mapper;

import com.xbjtech.vipsystem.po.PushmoneyConfig;

public interface PushmoneyConfigMapper extends BaseMapper<PushmoneyConfig> {
}