package com.xbjtech.vipsystem.mapper;

import com.xbjtech.vipsystem.po.Vip;

public interface VipMapper extends BaseMapper<Vip> {
}