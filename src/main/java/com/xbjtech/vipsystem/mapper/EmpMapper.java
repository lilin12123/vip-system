package com.xbjtech.vipsystem.mapper;

import com.xbjtech.vipsystem.po.Emp;

public interface EmpMapper extends BaseMapper<Emp> {
}