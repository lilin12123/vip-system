package com.xbjtech.vipsystem.mapper;


import com.xbjtech.vipsystem.dto.Menu;
import com.xbjtech.vipsystem.po.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {

    int deleteRoleRelationByUserId(Integer id);

    int insertRoleRelation(@Param("userId") Integer userId,
                           @Param("roleId") Integer roleId);

    List<Menu> selectMenus(String username);

}