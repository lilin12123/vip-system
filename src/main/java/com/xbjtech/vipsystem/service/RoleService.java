package com.xbjtech.vipsystem.service;

import com.xbjtech.vipsystem.mapper.RoleMapper;
import com.xbjtech.vipsystem.po.Role;
import com.xbjtech.vipsystem.vo.RoleOptVO;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class RoleService extends BaseService<Role> {

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    // 可以保证所有insert操作运行在一个事务内
    // 初始工程师
    // 中级工程师 可以会考虑到性能方面的问题

    // 根据业务场景 来判断是否会有性能问题
    // 考虑一个同一时间内用这个接口的人多不多，用的频率高不高
    public void insertRoleAndResRelation(RoleOptVO[] vos) {
        // 处理之前 检查一个vos里面的roleid是否是一样的，如果不一样，不进行处理
        // 1000 > 50
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
        RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
        try {
            if (null != vos && vos.length > 0) {
                roleMapper.deleteRoleRelation(vos[0].getRoleId());
                for (RoleOptVO vo : vos) {
                    roleMapper.insertRoleAndResRelation(vo.getRoleId(), vo.getResId());
                }
            }
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public List<Integer> selectResByRoleId(Integer roleId) {
        return roleMapper.selectResByRoleId(roleId);
    }

}