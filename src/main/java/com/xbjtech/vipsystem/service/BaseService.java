package com.xbjtech.vipsystem.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xbjtech.vipsystem.mapper.BaseMapper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 通用服务类
 *
 * @author leejun.happy@gmail.com
 * @created 2019/2/20
 */
public abstract class BaseService<T> {

    @Autowired
    public BaseMapper<T> mapper;

    public T selectOne(T t) {
        return mapper.selectOne(t);
    }

    public List<T> select(T t) {
        return mapper.select(t);
    }

    public List<T> selectAll() {
        return mapper.selectAll();
    }

    public int selectCount(T t) {
        return mapper.selectCount(t);
    }

    public T selectByPrimaryKey(Object key) {
        return mapper.selectByPrimaryKey(key);
    }

    public boolean existsWithPrimaryKey(Object key) {
        return mapper.existsWithPrimaryKey(key);
    }

    @Transactional
    public int insert(T record) {
        return mapper.insert(record);
    }

    @Transactional
    public int insertSelective(T record) {
        return mapper.insertSelective(record);
    }

    @Transactional
    public int updateByPrimaryKey(T record) {
        return mapper.updateByPrimaryKey(record);
    }

    @Transactional
    public int updateByPrimaryKeySelective(T record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Transactional
    public int delete(T record) {
        return mapper.delete(record);
    }

    @Transactional
    public int deleteByPrimaryKey(Object key) {
        return mapper.deleteByPrimaryKey(key);
    }

    public List<T> selectByExample(Object example) {
        return mapper.selectByExample(example);
    }

    public T selectOneByExample(Object example) {
        return mapper.selectOneByExample(example);
    }

    public int selectCountByExample(Object example) {
        return mapper.selectCountByExample(example);
    }

    @Transactional
    public int deleteByExample(Object example) {
        return mapper.deleteByExample(example);
    }

    @Transactional
    public int updateByExample(T record, Object example) {
        return mapper.updateByExample(record, example);
    }

    @Transactional
    public int updateByExampleSelective(T record, Object example) {
        return mapper.updateByExampleSelective(record, example);
    }

    public List<T> selectByExampleAndRowBounds(Object example, RowBounds rowBounds) {
        return mapper.selectByExampleAndRowBounds(example, rowBounds);
    }

    public List<T> selectByRowBounds(T record, RowBounds rowBounds) {
        return mapper.selectByRowBounds(record, rowBounds);
    }

    @Transactional
    public int insertList(List<? extends T> recordList) {
        return mapper.insertList(recordList);
    }

    @Transactional
    public int insertWithoutPrimaryKey(T record) {
        return mapper.insertUseGeneratedKeys(record);
    }

    public PageInfo<T> selectByExamplePage(int pageNum, int pageSize, Object example) {
        PageHelper.startPage(pageNum, pageSize);
        List<T> list = mapper.selectByExample(example);
        return new PageInfo<T>(list);
    }

}
