package com.xbjtech.vipsystem.service;

import com.xbjtech.vipsystem.mapper.ResourceMapper;
import com.xbjtech.vipsystem.po.Resource;
import com.xbjtech.vipsystem.vo.SelectTreeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResourceService extends BaseService<Resource> {

    @Autowired
    ResourceMapper resourceMapper;

    public List<SelectTreeData> selectTreeDate() {
        return resourceMapper.selectTreeDate();
    }

    public List<SelectTreeData> selectTreeDataThreeLevel() {
        return resourceMapper.selectTreeDataThreeLevel();
    }

}