package com.xbjtech.vipsystem.vo;

import lombok.Data;

import java.util.List;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-25
 */
@Data
public class SelectTreeData {

    private Integer id;
    private String label;
    private List<SelectTreeData> children;

}
