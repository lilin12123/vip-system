package com.xbjtech.vipsystem.vo;

import lombok.Data;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-26
 */
@Data
public class RoleOptVO {
    private Integer roleId;
    private Integer resId;
}
