package com.xbjtech.vipsystem.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-24
 */
@Data
public class LoginVO {

    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;
}
