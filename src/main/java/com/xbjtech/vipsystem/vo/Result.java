package com.xbjtech.vipsystem.vo;


import lombok.Data;

import java.util.List;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-23
 */
@Data
public class Result<T> {

    private boolean success;
    private String msg;
    private List<T> data;

    public Result() {
    }

    public Result(boolean success, String msg, List<T> data) {
        this.success = success;
        this.msg = msg;
        this.data = data;
    }

    public static Result<String> ok(String msg) {
        Result<String> result = new Result<>();
        result.setSuccess(true);
        result.setMsg(msg);
        return result;
    }

    public static Result<String> fail(String msg) {
        Result<String> result = new Result<>();
        result.setSuccess(false);
        result.setMsg(msg);
        return result;
    }


}
