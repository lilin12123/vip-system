package com.xbjtech.vipsystem.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-29
 */
@Data
public class UserOptVO {

    @NotNull
    private Integer userId;

    @NotNull
    private List<Integer> roleIds;
}
