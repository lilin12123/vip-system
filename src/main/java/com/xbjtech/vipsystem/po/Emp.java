package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "t_emp")
public class Emp {

    @Id
    private Integer id;

    private Integer empCategoryId;

    private String name;

    private String mobile;

    private String address;

    private BigDecimal selaray;

    private Date joinTime;

    private String shopId;

}