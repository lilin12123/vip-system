package com.xbjtech.vipsystem.po;


import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "t_emp_category")
public class EmpCategory {

    @Id
    private Integer id;

    private String category;

}