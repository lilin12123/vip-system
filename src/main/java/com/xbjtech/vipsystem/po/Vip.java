package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "t_vip")
public class Vip {

    @Id
    private Integer id;

    private Integer vipLevelId;

    private String name;

    private String vipNo;

    private String idCode;

    private String mobile;

    private Date createTime;

    private Date birth;

    private BigDecimal balance;

}