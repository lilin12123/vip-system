package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Table(name = "t_emp_pushmoney_log")
public class EmpPushmoneyLog {

    @Id
    private Integer id;

    private Integer empId;

    private BigDecimal amount;

    private String createTime;

    private String rechargeOrConsumeLogId;

}