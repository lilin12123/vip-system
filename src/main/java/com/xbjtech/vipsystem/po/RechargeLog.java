package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "t_recharge_log")
public class RechargeLog {

    @Id
    private Integer id;

    private Integer vipId;

    private String orderNo;

    private BigDecimal rechargeAmount;

    private BigDecimal giftAmount;

    private BigDecimal sumAmount;

    private Date createTime;

}