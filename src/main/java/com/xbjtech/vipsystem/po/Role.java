package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "t_role")
public class Role {

    @Id
    private Integer id;

    private String roleName;

    private String roleCode;

    private String description;

}