package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Table(name = "t_pushmoney_config")
public class PushmoneyConfig {

    @Id
    private Integer id;

    private Integer type;

    private BigDecimal value;

}