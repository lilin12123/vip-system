package com.xbjtech.vipsystem.po;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

@Data
@Table(name = "t_resource")
public class Resource {

    @Id
    private Integer id;

    @JsonProperty(value = "title")
    @NotBlank(message = "权限名称不能为空")
    private String resName;

    private String resCode;

    @JsonProperty("pid")
    private Integer parId = new Integer(0);

    private String icon;

    private String url;

    // 0 表示按钮 1表示菜单
    private Integer type = 0;

    @Transient
    private String pvalue;

}