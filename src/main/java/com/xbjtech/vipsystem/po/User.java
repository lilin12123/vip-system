package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Table(name = "t_user")
public class User {

    @Id
    private Integer id;

    @NotBlank
    private String username;

    private String password;

    private String mobile;

    private String shopId;

    private Date createTime;

    private Date lastLoginTime;

    private String lastLoginIp;

    private Integer deleted;

    private String shopName;

    private String shopAddress;

    private Integer headId;
}