package com.xbjtech.vipsystem.po;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Table(name = "t_vip_level")
public class VipLevel {

    @Id
    private Integer id;

    private String name;

    private BigDecimal discount;

}