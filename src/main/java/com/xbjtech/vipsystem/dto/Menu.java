package com.xbjtech.vipsystem.dto;

import lombok.Data;

import java.util.List;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-31
 */
@Data
public class Menu {

    private Integer id;
    private String title;
    private String path;
    private String icon;
    private Integer pid;
    private List<Menu> children;

}
