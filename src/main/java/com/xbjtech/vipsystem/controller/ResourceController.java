package com.xbjtech.vipsystem.controller;

import com.xbjtech.vipsystem.po.Resource;
import com.xbjtech.vipsystem.service.ResourceService;
import com.xbjtech.vipsystem.utils.ArgumentResolver;
import com.xbjtech.vipsystem.vo.Result;
import com.xbjtech.vipsystem.vo.SelectTreeData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-24
 */
@RestController
@RequestMapping("/res")
@Api
@Slf4j
public class ResourceController {

    @Autowired
    ResourceService resourceService;

    /**
     *  权限管理模块
     *  管理项目涉及的所有权限
     *
     *  用户管理  -> 用户列表
     *           -> 新增
     *           -> 修改
     *           -> 删除
     *           ...
     *  会员管理  -> 会员列表
     *           -> 会员新增
     *           -> 会员修改
     *           -> 会员删除
     *  不启用分页
      */
    @GetMapping("/data")
    public List<Resource> treeTableData() {
        List<Resource> resources = resourceService.selectAll();
        return resources;
    }

    /**
     * application/json
     * 处理新增
     */
    @ApiOperation(value="处理新增和修改保存操作", notes = "{\n" +
            "  \"icon\": \"&#xe609;\",\n" +
            "  \"pid\": 1,\n" +
            "  \"resCode\": \"user:list\",\n" +
            "  \"title\": \"用户管理\",\n" +
            "  \"url\": \"/user/list\"\n" +
            "}")
    @PostMapping("/save")
    public Result<String> save(@RequestBody @Validated Resource resource) {
        try {
            if (resource.getId() == null) {
                resourceService.insertWithoutPrimaryKey(resource);
            } else {
                Resource basic = resourceService.selectByPrimaryKey(resource.getId());
                ArgumentResolver<Resource> resolver = new ArgumentResolver<>();
                Resource result = resolver.resolver(basic, resource, Resource.class);
                resourceService.updateByPrimaryKey(result);
            }
            return Result.ok("保存成功");
        } catch (Exception e) {
            log.error("{}", e.getMessage());
        }
        return Result.fail("保存失败");
    }

    @GetMapping("/delete/{id}")
    public Result<String> del(@PathVariable Integer id) {
        try {
            resourceService.deleteByPrimaryKey(id);
            return Result.ok("删除成功");
        } catch (Exception e) {
            log.error("{}", e.getMessage());
        }
        return Result.fail("删除失败");
    }

    @GetMapping("/selectTreeData")
    public List<SelectTreeData> selectTreeData() {
        return resourceService.selectTreeDate();
    }

    @GetMapping("/get/{id}")
    public Resource selectResourceById(@PathVariable Integer id) {
        Resource resource = resourceService.selectByPrimaryKey(id);
        Resource parent = resourceService.selectByPrimaryKey(resource.getParId());
        if (parent != null) {
            resource.setPvalue(parent.getResName());
        }
        return resource;
    }

    @GetMapping("/selectTreeDataThreeLevel")
    public List<SelectTreeData> selectTreeDataThreeLevel() {
        return resourceService.selectTreeDataThreeLevel();
    }

}
