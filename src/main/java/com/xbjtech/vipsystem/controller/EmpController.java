package com.xbjtech.vipsystem.controller;

import com.github.pagehelper.PageInfo;
import com.xbjtech.vipsystem.po.Emp;
import com.xbjtech.vipsystem.po.Role;
import com.xbjtech.vipsystem.po.User;
import com.xbjtech.vipsystem.service.EmpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.Map;

/**
 * @author leejun.happy@gmail.com
 * @created 2019-07-31
 */
@RestController
@Slf4j
@Api
@RequestMapping("/emp")
public class EmpController {

    @Autowired
    EmpService empService;

    @ApiOperation(value = "获取员工列表分页数据")
    @GetMapping("/data")
    public Map<String, Object> data(
            @RequestParam(value = "page", required = false, defaultValue = "1")
                    int page,
            @RequestParam(value = "limit", required = false, defaultValue = "10")
                    int limit) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        Example ex = new Example(Emp.class);
        ex.setOrderByClause("id desc");
        ex.createCriteria().andEqualTo("shopId", user.getShopId());
        PageInfo<Emp> rolePageInfo = empService.selectByExamplePage(page, limit, ex);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", rolePageInfo.getTotal());
        map.put("data", rolePageInfo.getList());
        return map;
    }

}
