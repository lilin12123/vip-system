/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : vip_system

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 09/08/2019 10:34:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_ activity
-- ----------------------------
DROP TABLE IF EXISTS `t_ activity`;
CREATE TABLE `t_ activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `recharge_amount` decimal(10,2) DEFAULT NULL,
  `gift_amount` decimal(10,2) DEFAULT NULL,
  `repeatable` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_ consume_log
-- ----------------------------
DROP TABLE IF EXISTS `t_ consume_log`;
CREATE TABLE `t_ consume_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(45) NOT NULL,
  `vip_id` int(11) DEFAULT NULL,
  `consume_amount` decimal(10,2) NOT NULL,
  `discount_amount` decimal(10,2) NOT NULL,
  `after_discount_amount` decimal(10,2) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_no_UNIQUE` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_ recharge_log
-- ----------------------------
DROP TABLE IF EXISTS `t_ recharge_log`;
CREATE TABLE `t_ recharge_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(45) NOT NULL,
  `vip_id` int(11) NOT NULL,
  `recharge_amount` decimal(10,2) NOT NULL,
  `gift_amount` decimal(10,2) NOT NULL,
  `sum_amount` decimal(10,2) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`,`vip_id`),
  UNIQUE KEY `order_no_UNIQUE` (`order_no`),
  KEY `fk_t_ recharge_log_t_vip1_idx` (`vip_id`),
  CONSTRAINT `fk_t_ recharge_log_t_vip1` FOREIGN KEY (`vip_id`) REFERENCES `t_vip` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_emp
-- ----------------------------
DROP TABLE IF EXISTS `t_emp`;
CREATE TABLE `t_emp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `emp_category_id` int(11) NOT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `selaray` decimal(10,2) DEFAULT NULL,
  `join_time` datetime DEFAULT NULL,
  `shop_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`,`emp_category_id`),
  KEY `fk_t_emp_t_emp_category1_idx` (`emp_category_id`),
  CONSTRAINT `fk_t_emp_t_emp_category1` FOREIGN KEY (`emp_category_id`) REFERENCES `t_emp_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_emp
-- ----------------------------
BEGIN;
INSERT INTO `t_emp` VALUES (1, '张三', 2, '13000000000', '安徽省合肥市', 10000.00, '2018-01-01 00:00:00', '1');
INSERT INTO `t_emp` VALUES (2, '李四', 3, NULL, NULL, NULL, NULL, '001');
COMMIT;

-- ----------------------------
-- Table structure for t_emp_category
-- ----------------------------
DROP TABLE IF EXISTS `t_emp_category`;
CREATE TABLE `t_emp_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL,
  `shop_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_emp_category
-- ----------------------------
BEGIN;
INSERT INTO `t_emp_category` VALUES (1, '前台', '1');
INSERT INTO `t_emp_category` VALUES (2, '销售', '1');
INSERT INTO `t_emp_category` VALUES (3, '前台', '2');
COMMIT;

-- ----------------------------
-- Table structure for t_emp_category_has_t_pushmoney_config
-- ----------------------------
DROP TABLE IF EXISTS `t_emp_category_has_t_pushmoney_config`;
CREATE TABLE `t_emp_category_has_t_pushmoney_config` (
  `emp_category_id` int(11) NOT NULL,
  `pushmoney_config_id` int(11) NOT NULL,
  PRIMARY KEY (`emp_category_id`,`pushmoney_config_id`),
  KEY `fk_t_emp_category_has_t_pushmoney_config_t_pushmoney_config_idx` (`pushmoney_config_id`),
  KEY `fk_t_emp_category_has_t_pushmoney_config_t_emp_category1_idx` (`emp_category_id`),
  CONSTRAINT `fk_t_emp_category_has_t_pushmoney_config_t_emp_category1` FOREIGN KEY (`emp_category_id`) REFERENCES `t_emp_category` (`id`),
  CONSTRAINT `fk_t_emp_category_has_t_pushmoney_config_t_pushmoney_config1` FOREIGN KEY (`pushmoney_config_id`) REFERENCES `t_pushmoney_config` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_emp_pushmoney_log
-- ----------------------------
DROP TABLE IF EXISTS `t_emp_pushmoney_log`;
CREATE TABLE `t_emp_pushmoney_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `create_time` varchar(45) NOT NULL,
  `recharge_or_consume_log_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`,`emp_id`),
  KEY `fk_t_emp_pushmoney_log_t_emp1_idx` (`emp_id`),
  CONSTRAINT `fk_t_emp_pushmoney_log_t_emp1` FOREIGN KEY (`emp_id`) REFERENCES `t_emp` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_pushmoney_config
-- ----------------------------
DROP TABLE IF EXISTS `t_pushmoney_config`;
CREATE TABLE `t_pushmoney_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) DEFAULT NULL COMMENT '提成的类别\n1. 表示百分比提成\n2. 按次提成固定金额',
  `value` decimal(10,2) DEFAULT NULL COMMENT 'type = 1    表示百分比提成，那么value应该是一个百分比\ntype = 2    表示按次提成，那边value是一个固定金额     ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_resource`;
CREATE TABLE `t_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `res_name` varchar(45) NOT NULL,
  `res_code` varchar(45) DEFAULT NULL,
  `par_id` int(11) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of t_resource
-- ----------------------------
BEGIN;
INSERT INTO `t_resource` VALUES (1, '系统管理', 'setting', 0, '&#xe631;', '', 1);
INSERT INTO `t_resource` VALUES (2, '权限管理', 'res:list', 1, '&#xe60c;', '#/res/list', 1);
INSERT INTO `t_resource` VALUES (3, '新增', 'res:add', 2, '&#xe60c; ', '', 0);
INSERT INTO `t_resource` VALUES (4, '修改', 'res:edit', 2, '&#xe60c; ', '', 0);
INSERT INTO `t_resource` VALUES (5, '删除', 'res:del', 2, '&#xe60c; ', '', 0);
INSERT INTO `t_resource` VALUES (6, '角色管理', 'role:list', 1, '&#xe60c; ', '#/role/list', 1);
INSERT INTO `t_resource` VALUES (7, '新增', 'role:add', 6, '&#xe60c; ', '', 0);
INSERT INTO `t_resource` VALUES (8, '修改', 'role:edit', 6, '&#xe60c; ', '', 0);
INSERT INTO `t_resource` VALUES (9, '删除', 'role:del', 6, '&#xe60c; ', '', 0);
INSERT INTO `t_resource` VALUES (17, '权限分配', 'role:opt', 6, '&#xe654;', '', 0);
INSERT INTO `t_resource` VALUES (18, '用户管理', 'user:list', 1, '&#xe66f;', '#/user/list', 1);
INSERT INTO `t_resource` VALUES (19, '新增', 'user:add', 18, '&#xe654;', '', 0);
INSERT INTO `t_resource` VALUES (20, '修改', 'user:edit', 18, '&#xe654;', '', 0);
INSERT INTO `t_resource` VALUES (21, '删除', 'user:remove', 18, '&#xe654;', '', 0);
INSERT INTO `t_resource` VALUES (22, '角色分配', 'user:opt', 18, '&#xe654;', '', 0);
INSERT INTO `t_resource` VALUES (23, '企业管理', 'qyma', 0, '企', '1', 0);
INSERT INTO `t_resource` VALUES (24, '员工管理', 'emp:list', 0, '&#xe770;', '#/emp/list', 1);
COMMIT;

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL,
  `role_code` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
BEGIN;
INSERT INTO `t_role` VALUES (207, '超级管理员', 'admin', '');
INSERT INTO `t_role` VALUES (208, '店铺', 'shop', '');
INSERT INTO `t_role` VALUES (209, '买家', 'buyer', '');
INSERT INTO `t_role` VALUES (210, '卖家', 'seller', '');
COMMIT;

-- ----------------------------
-- Table structure for t_role_has_t_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_role_has_t_resource`;
CREATE TABLE `t_role_has_t_resource` (
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`resource_id`),
  KEY `fk_t_role_has_t_resource_t_resource1_idx` (`resource_id`),
  KEY `fk_t_role_has_t_resource_t_role1_idx` (`role_id`),
  CONSTRAINT `fk_t_role_has_t_resource_t_resource1` FOREIGN KEY (`resource_id`) REFERENCES `t_resource` (`id`),
  CONSTRAINT `fk_t_role_has_t_resource_t_role1` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_has_t_resource
-- ----------------------------
BEGIN;
INSERT INTO `t_role_has_t_resource` VALUES (207, 1);
INSERT INTO `t_role_has_t_resource` VALUES (209, 1);
INSERT INTO `t_role_has_t_resource` VALUES (210, 1);
INSERT INTO `t_role_has_t_resource` VALUES (207, 2);
INSERT INTO `t_role_has_t_resource` VALUES (209, 2);
INSERT INTO `t_role_has_t_resource` VALUES (210, 2);
INSERT INTO `t_role_has_t_resource` VALUES (207, 3);
INSERT INTO `t_role_has_t_resource` VALUES (209, 3);
INSERT INTO `t_role_has_t_resource` VALUES (210, 3);
INSERT INTO `t_role_has_t_resource` VALUES (207, 4);
INSERT INTO `t_role_has_t_resource` VALUES (209, 4);
INSERT INTO `t_role_has_t_resource` VALUES (210, 4);
INSERT INTO `t_role_has_t_resource` VALUES (207, 5);
INSERT INTO `t_role_has_t_resource` VALUES (209, 5);
INSERT INTO `t_role_has_t_resource` VALUES (210, 5);
INSERT INTO `t_role_has_t_resource` VALUES (207, 6);
INSERT INTO `t_role_has_t_resource` VALUES (210, 6);
INSERT INTO `t_role_has_t_resource` VALUES (207, 7);
INSERT INTO `t_role_has_t_resource` VALUES (210, 7);
INSERT INTO `t_role_has_t_resource` VALUES (207, 8);
INSERT INTO `t_role_has_t_resource` VALUES (210, 8);
INSERT INTO `t_role_has_t_resource` VALUES (207, 9);
INSERT INTO `t_role_has_t_resource` VALUES (210, 9);
INSERT INTO `t_role_has_t_resource` VALUES (207, 17);
INSERT INTO `t_role_has_t_resource` VALUES (210, 17);
INSERT INTO `t_role_has_t_resource` VALUES (207, 18);
INSERT INTO `t_role_has_t_resource` VALUES (210, 18);
INSERT INTO `t_role_has_t_resource` VALUES (207, 19);
INSERT INTO `t_role_has_t_resource` VALUES (210, 19);
INSERT INTO `t_role_has_t_resource` VALUES (207, 20);
INSERT INTO `t_role_has_t_resource` VALUES (210, 20);
INSERT INTO `t_role_has_t_resource` VALUES (207, 21);
INSERT INTO `t_role_has_t_resource` VALUES (210, 21);
INSERT INTO `t_role_has_t_resource` VALUES (207, 22);
INSERT INTO `t_role_has_t_resource` VALUES (210, 22);
INSERT INTO `t_role_has_t_resource` VALUES (207, 24);
COMMIT;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(30) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `shop_id` varchar(45) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `last_login_ip` varchar(50) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '0:   没有删除\n1：已经删除',
  `shop_name` varchar(50) DEFAULT NULL,
  `shop_address` varchar(200) DEFAULT NULL,
  `head_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `shop_id_UNIQUE` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
BEGIN;
INSERT INTO `t_user` VALUES (1, 'admin', '123456', '15922333447', '1', '2019-01-01 08:20:56', NULL, NULL, 0, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (4, 'buyer', '123456', '15922333447', '001', '2019-07-31 15:14:35', NULL, NULL, 0, '买家', '1', 0);
INSERT INTO `t_user` VALUES (5, 'seller', '123456', '1', '2', '2019-07-31 15:14:55', NULL, NULL, 0, '3', '4', 0);
COMMIT;

-- ----------------------------
-- Table structure for t_user_has_t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_has_t_role`;
CREATE TABLE `t_user_has_t_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_t_user_has_t_role_t_role1_idx` (`role_id`),
  KEY `fk_t_user_has_t_role_t_user_idx` (`user_id`),
  CONSTRAINT `fk_t_user_has_t_role_t_role1` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),
  CONSTRAINT `fk_t_user_has_t_role_t_user` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_has_t_role
-- ----------------------------
BEGIN;
INSERT INTO `t_user_has_t_role` VALUES (1, 207);
INSERT INTO `t_user_has_t_role` VALUES (4, 209);
INSERT INTO `t_user_has_t_role` VALUES (5, 210);
COMMIT;

-- ----------------------------
-- Table structure for t_vip
-- ----------------------------
DROP TABLE IF EXISTS `t_vip`;
CREATE TABLE `t_vip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vip_level_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `vip_no` varchar(45) NOT NULL,
  `id_code` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `balance` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vip_no_UNIQUE` (`vip_no`),
  UNIQUE KEY `id_code_UNIQUE` (`id_code`),
  KEY `fk_t_vip_t_vip_level1` (`vip_level_id`),
  CONSTRAINT `fk_t_vip_t_vip_level1` FOREIGN KEY (`vip_level_id`) REFERENCES `t_vip_level` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_vip_level
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_level`;
CREATE TABLE `t_vip_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
